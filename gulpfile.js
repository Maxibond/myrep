var gulp = require('gulp'),
    browserify = require('browserify'),
    browserSync = require('browser-sync'),
    reload = browserSync.reload,
    less = require('gulp-less'),
    concat = require('gulp-concat'),
    minifyCSS = require('gulp-minify-css'), 
    rename = require("gulp-rename"),
    util = require('gulp-util'),
    clean = require('gulp-clean');

gulp.task('less', function() {
      return gulp.src('src/less/main.less')
       .pipe(less())
        .pipe(gulp.dest('app/styles'))
        .pipe(browserSync.reload({stream:true}));
    });

gulp.task('scripts', function() {
    gulp.src('app/scripts/main.js')
        .pipe(clean());
    gulp.src('src/js/*.js')
    .pipe(concat('main.js'))
    .pipe(gulp.dest('app/scripts'));
});

gulp.task('default', ['serve']);

// watch files for changes and reload
gulp.task('serve', ['less', 'scripts'], function() {
  browserSync.init({
    server: "app"
  })
  gulp.watch('src/js/*.js', ['scripts'])
  gulp.watch('src/less/*.less', ['less']);
  gulp.watch(['*.html', 'styles/*.css', 'scripts/*.js'], {cwd: 'app'}, reload);
});

gulp.task('default', ['serve']);